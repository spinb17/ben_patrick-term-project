""" @file proj.py
    @brief This file contains a ball balancing controller
    @details The program utilizes a task scheduler program called cotask.py to 
    run tasks at regular intervals in order to control two motors actuating a table
    with a ball on it.The control method is state feedback control. 
    
    Source Code: https://bitbucket.org/spinb17/ben_patrick-term-project/src/master/proj.py

    @author Ben Spin and Patrick Ward
"""

import pyb
from micropython import const, alloc_emergency_exception_buf
import gc

import cotask
import task_share

from pyb import Pin

import utime

import math

from Encoder import Encoder

from Motor_Driver import MotorDriver

from TouchPad_Driver import TouchDriver

import array

## Contains a motor driver object that operates both motors on our board
moe = MotorDriver(Pin.cpu.A15 , Pin.cpu.B0 , Pin.cpu.B1 ,Pin.cpu.B4,Pin.cpu.B5, 3 , 10)  

## Stores a touch driver object passing in the pins and lengths
touch = TouchDriver(Pin.board.PA7, Pin.board.PA1, Pin.board.PA0, Pin.board.PA6 , .172 , .098 , .107 , .053)

## Stores the Encoder object for the second motor
Enc2 = Encoder(Pin.cpu.C6, Pin.cpu.C7, 8)

## Stores the Encoder object for the first motor
Enc1 = Encoder(Pin.cpu.B6, Pin.cpu.B7, 4)

## Start time for plotting data
startTime = utime.ticks_us()

# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)

# Declare some constants to make state machine code a little more readable.
# This is optional; some programmers prefer to use numbers to identify tasks
## inital state for the Touch_task
START = const(2)
## Normal running state for the Touch_task when the ball has been placed on the touchpad
RUN = const(3)

## Gain for ball position along x-axis
k11 = const(110)

## Gain for ball velocity along x-axis
k31 = const(2)

## Gain for angle of table about the y-axis
k21 = const(10)

## Gain for angular velocity of the table about the y-axis
k41 = 1.8

## Gain for the ball position along the y-axis
k12 = const(101)

## Gain for the ball velocity along the y-axis
k32 = const(2)

## Gain for the table angle about the x-axis
k22 = 14.3

## Gain for the table angular velocity about the x-axis
k42 = 2.2

## For storing time values
time_val = array.array('L', [])

## For storing table theta values about the y-axis
thetay = array.array('f', [])

## For storing table theta valuies about the x-axis
thetax = array.array('f', [])

## For storing table angular velocities about the x-axis
thetadotx = array.array('f', [])

## For storing table angular velocities about the y-axis
thetadoty = array.array('f',[])

def Yaxis_task ():
    """ @brief  Controls the balance of the table about the y-axis changing the angle of the x-axis
        @details This task gets the angular data from our Enc_task and the ball data from
        the Touch_task. The four data types are multiplied by our tuned gains to generate
        a torque that is then converted into the coorilating duty cycle.
    """

    while True:
        
        ## Contains the current angular data of the table about the y-axis
        th_y = thy.get(False)
        
        ## Contains the current angular velocity data of the table about the y-axis
        thd_y = thdy.get(False)
        
        ## Contains the current ball position along the x-axis
        x_ = x.get(False)
        
        ## Contains the current ball velocity along the x-axis
        x_dot = xd.get(False)
        
        ## Torque generated to balance the table and ball
        Tx = (-k11*x_) + (k21*th_y) + (-k31*x_dot) + (k41*thd_y)
        
        ## Duty cycle for the motor to apply the requested torque
        Lx = (Tx*(2.21)/(12*13.8))*1000  #Tx-[N-m] R-2.21[ohms] Vdc - 12 [V] kt- 13.8 [mN-m/A]
        
        #print(str(Lx) + 'Yaxis motor')
        
        # setting the duty cycle for the motor
        moe.set_duty(Lx,2)

        yield (0)

def Xaxis_task ():
    """ @brief  Controls the balance of the table about the x-axis changing the angle of the y-axis
        @details This task gets the angular data from our Enc_task and the ball data from
        the Touch_task. The four data types are multiplied by our tuned gains to generate
        a torque that is then converted into the coorilating duty cycle.
    """

    while True:
        
        ## Contains the current angular data of the table about the x-axis
        th_x = thx.get(False)
        
        ## Contains the current angular velocity data of the table about the x-axis
        thd_x = thdx.get(False)
        
        ## Contains the current ball position along the y-axis
        y_ = y.get(False)
        
        ## Contains the current ball velocity along the y-axis
        y_dot = yd.get(False)
        
        ## Torque generated to balance the table and ball
        Ty = (k12*y_) + (k22*th_x) + (k32*y_dot) + (k42*thd_x)
        
        ## Duty cycle for the motor to apply the requested torque
        Ly = (Ty*(2.21)/(12*13.8))*1000  #Tx-[N-m] R-2.21[ohms] Vdc - 12 [V] kt- 13.8 [mN-m/A]
        
        #print(str(Ly) + 'Xaxis motor')
        
        # setting the duty cycle for the motor
        moe.set_duty(Ly,1)

        yield (0)
        
def Enc_task ():
    """ @brief Updates the encoders at a regular interval and record data
        @details This task records the angular data for both motor encoders and 
        stores it in the appropriate variables
    """
    
    while True:
        ## Horizontal Distance from U-Joint to Push-Rod Pivot
        lp =0.11 # [m]
        ## Radius of Lever Arm
        rm = 0.06 # [m]
        
        # Updates the encoders
        Enc1.update()
        Enc2.update()
        
        # Converts to radians per second using 4000 PPR
        phi_y_dot = (Enc1.get_delta()/0.1)*(2*math.pi/4000)
        phi_x_dot = (Enc2.get_delta()/0.1)*(2*math.pi/4000)
        
        # Converts to radians using 4000 PPR
        phi_y = Enc1.position*(2*math.pi/4000)
        phi_x = Enc2.position*(2*math.pi/4000)
        
        # converts form encoder angle to table angle
        th_x = phi_y*(-lp/rm)
        th_y = phi_x*(-lp/rm)
        
        # converts from encoder angular velocity to table angular velocity
        thd_x = phi_y_dot*(-lp/rm)
        thd_y = phi_x_dot*(-lp/rm)
        
        # Stores all the data for plotting
        time_val.append(utime.ticks_diff(utime.ticks_us(),startTime))
        thetay.append(th_y)
        thetax.append(th_x)
        thetadotx.append(thd_x)
        thetadoty.append(thd_y)
        
        # Stores individual values for controller use
        thy.put(th_y, False)
        thx.put(th_x, False)
        thdx.put(thd_x, False)
        thdy.put(thd_y, False)
        
        yield (0)
        
def Touch_task ():
    """ @brief Finite state machine for a touchpad
        @details This task will run with no output if the touchpad is never
        touched. Once the touchpad is touched, it begins recording position and
        velocity for the ball.
    """
    ## Contains the current state of the task
    state = START
    
    while True:
        
        # Checks State
        if state == START:
            
            # Pulls values from the touchpad
            ## Storage for initial touchpad values
            initial = touch.three_comp()
            
            # Checks if contact is true
            if initial[2] == True:
                
                # Sets initial values for the ball
                ## Initial x position of the ball
                xlast = initial[0]
                ## Initial y position of the ball
                ylast = initial[1]
                
                # switches state to normal operation
                state = RUN
                
            # If contact is not true then set all the x and y values equal to 0
            else:
                x.put(0)
                y.put(0)
                xd.put(0)
                yd.put(0)
        
        elif state == RUN:
            
            ## Temporarily holds all the touchpad data
            values = touch.three_comp()
        
            # If contact is true update old values
            if values[2] == True:
                ## x position of the ball
                x_ = values[0]
                
                ## y position of the ball
                y_ = values[1]
                
                ## x velocity of the ball
                x_dot = (x_ - xlast)/0.01
                
                ## y velocity of the ball
                y_dot = (y_ - ylast)/0.01
            
                xlast = x_
                ylast = y_
                
                # storing all the values for use in other tasks
                x.put(x_)
                y.put(y_)
                xd.put(x_dot)
                yd.put(y_dot)
            else:
                pass
        else:
            pass
        
        yield (state)
        
def Test ():
    """ @brief simple test task 
    """

    while True:
        
        print(str(thy.get(False)))
        
        yield (0)
        



# =============================================================================

if __name__ == "__main__":

    ## Contains the current angular data of the table about the y-axis
    thy = task_share.Share ('f', thread_protect = False, name = "thy")
    ## Contains the current angular velocity data of the table about the y-axis
    thdy = task_share.Share ('f', thread_protect = False, name = "thdy")
    
    ## Contains the current angular data of the table about the x-axis
    thx = task_share.Share ('f', thread_protect = False, name = "thx")
    ## Contains the current angular velocity data of the table about the x-axis
    thdx = task_share.Share ('f', thread_protect = False, name = "thdx")
    
    ## Contains the current ball position along the x-axis
    x = task_share.Share ('f', thread_protect = False, name = "x")
    ## Contains the current ball position along the y-axis
    y = task_share.Share ('f', thread_protect = False, name = "y")
    
    ## Contains the current ball velocity along the x-axis
    xd = task_share.Share ('f', thread_protect = False, name = "xd")
    ## Contains the current ball velocity along the y-axis
    yd = task_share.Share ('f', thread_protect = False, name = "yd")
    

    # Create the tasks. If trace is enabled for any task, memory will be
    # allocated for state transition tracing, and the application will run out
    # of memory after a while and quit. Therefore, use tracing only for 
    # debugging and set trace to False when it's not needed
    ## Yaxis_task
    task1 = cotask.Task (Yaxis_task, name = 'Yaxis', priority = 1, 
                          period = 10, profile = True, trace = False)
    ## Xaxis_task
    task2 = cotask.Task (Xaxis_task, name = 'Xaxis', priority = 1, 
                          period = 10, profile = True, trace = False)
    ## Encoder task
    task3 = cotask.Task (Enc_task, name = 'Enc', priority = 2, 
                         period = 10, profile = True, trace = False)
    ## task4 = cotask.Task (Test, name = 'Task_2', priority = 2, 
    ##                      period = 100, profile = True, trace = False)
    
    ## Touchpad task
    task5 = cotask.Task (Touch_task, name = 'Touch', priority = 2,
                          period = 10, profile = True, trace = False)

    cotask.task_list.append (task1)
    cotask.task_list.append (task2)
    cotask.task_list.append (task3)
    ## cotask.task_list.append (task4)
    cotask.task_list.append (task5)
    

    # A task which prints characters from a queue has automatically been
    # created in print_task.py; it is accessed by print_task.put_bytes()


    # Run the memory garbage collector to ensure memory is as defragmented as
    # possible before the real-time scheduler is started
    gc.collect ()

    # Run the scheduler with the chosen scheduling algorithm. Quit if any 
    # character is sent through the serial port
    ## VCP object
    vcp = pyb.USB_VCP ()
    while not vcp.any ():
        cotask.task_list.pri_sched ()

    # Empty the comm port buffer of the character(s) just pressed
    vcp.read ()

    # Print a table of task data and a table of shared information data
    print ('\n' + str (cotask.task_list) + '\n')
    print (task_share.show_all ())
    print (task3.get_trace ())
    print ('\r\n')
    
    # For recording data
    with open('project.csv', 'w') as file:
        ## indexing variable
        i = 0
        while i<len(thetay):
            file.write('{:}, {:}, {:}, {:}, {:}\r'.format(time_val[i]/1e6, thetay[i], thetadoty[i], thetax[i], thetadotx[i]))
            #file.write('{:}, {:}, {:}\r'.format(time_val[i]/1e6, thetay[i], thetax[i]))
            i = i+1


'''
@file Motor_Driver.py

@brief This file contains a Dual Motor Driver

@details This file takes in all the pin callouts for both motors on our board. This 
driver includes two interupts: one for faults and one for resetting the motors. The class
contains methods reboot, nfault_handle, enable, disable, and set_duty.

source code can be found here: https://bitbucket.org/spinb17/ben_patrick-term-project/src/master/Motor_Driver.py

@author Ben Spin / Patrick Ward
@date March 5 , 2021
'''
import pyb
import utime

class MotorDriver:
    ''' 
    @brief This class implements a motor driver for the Balancing Platform Board
    '''
    def __init__ (self, nSLEEP_pin  , IN1_pin , IN2_pin , IN3_pin, IN4_pin, timer ,  min_dut):
        '''
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety
        
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin
        @param IN1_pin    A pyb.Pin object to use as the input to half bridge 1 for motor 1
        @param IN2_pin    A pyb.Pin object to use as the input to half bridge 2 for motor 1
        @param IN3_pin    A pyb.Pin object to use as the input to half bridge 1 for motor 2
        @param IN4_pin    A pyb.Pin object to use as the input to half bridge 2 for motor 2
        @param timer      A pyb.Timer object to use for the PWM generation on all IN pins.
        @param motor_num  An object used to define which motor the class object is meant to interact with
        @param min_dut    A minimum number as determined by the minimum speed at which the motor is able to function
        '''
        
        ## Determines the timer number
        self.Timer = timer
        ## Sleep pin sets up the pin that is used to toggle enableing the motor
        self.nSLEEP_pin = pyb.Pin (nSLEEP_pin, pyb.Pin.OUT_PP)
        ## Determines the first pin for the motor object
        self.IN1_pin = pyb.Pin(IN1_pin)
        ## Determines the seccond pin for the motor object
        self.IN2_pin = pyb.Pin(IN2_pin)
        ## Determines the first pin for the second motor object
        self.IN3_pin = pyb.Pin(IN3_pin)
        ## Determines the second pin for the second motor object
        self.IN4_pin = pyb.Pin(IN4_pin)
        ## Defines the minimum duty at which the motor is to operate at
        self.min_dut = min_dut
        ## Sets up the timer for the motor
        self.tim = pyb.Timer(self.Timer, freq= 20000)
        ## All variables to be input for the first channel
        self.t3ch1 = self.tim.channel(1, pyb.Timer.PWM , pin= self.IN1_pin)
        ## All variables to be input for the seccond cahnnel
        self.t3ch2 = self.tim.channel(2, pyb.Timer.PWM , pin= self.IN2_pin)
        ## All variables to be input for the third cahnnel 
        self.t3ch3 = self.tim.channel(3, pyb.Timer.PWM , pin= self.IN3_pin)
        ## All variables to be input for the fourth cahnnel
        self.t3ch4 = self.tim.channel(4, pyb.Timer.PWM , pin= self.IN4_pin)
        
        
        self.nSLEEP_pin.low()
        
        ## Interupt setup on the nfault pin as per the instructions in Lab08    
        self.extint = pyb.ExtInt (pyb.Pin.board.PB2,       # Which Pin
                            pyb.ExtInt.IRQ_FALLING,  # Interupt on falling edge
                            pyb.Pin.PULL_UP,         # Activate pullup resistor
                            self.nfault_handle)           # Interrupt service routine
        
        ## Interupt setup on the blue button pin
        self.extint1 = pyb.ExtInt (pyb.Pin.board.PC13,    # Which Pin
                            pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
                            pyb.Pin.PULL_UP,             # Activate pullup resistor
                            self.reboot)                   # Interrupt service routine)

    def nfault_handle(self, variable):
        '''
        @brief Interupt for disabling the motors and clears motor information
        '''
    
        self.disable()
        self.set_duty(0,1)
        self.set_duty(0,2)
    
    def reboot(self, nonsense):
        '''
        @brief Interupt for enabling the motor again after pushing the blue button
        '''
        
        self.enable()
    
        
    def enable (self):
        '''
        @brief Enables the motor by setting the sleep pin to high

        '''
        
        self.extint.disable()
        self.nSLEEP_pin.high()
        utime.sleep_us(3000)
        self.extint.enable()

        
    def disable(self):
        '''
        @brief Disables the motor by setting the sleep pin to low

        '''
        self.nSLEEP_pin.low()
        
    def set_duty(self, duty, moe):
        '''
        @brief Sets the duty cycle of the motor with positive values indicating forward direction and negative values indicating backwards direction
        @param duty a signed integer holding the duty cucle of the PWM signal sent to the motor
        @param moe a motor ID
        '''
        ## Motor ID
        if moe == 1: # checks motor id to see which motor to set duty cycle
            
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(0)
            
            if duty >= 100: # checks input duty to make sure whithin range
                self.t3ch1.pulse_width_percent(100)
                self.t3ch2.pulse_width_percent(0)
                
            elif duty <= -100:
                self.t3ch2.pulse_width_percent(100)
                self.t3ch1.pulse_width_percent(0)
                
            elif duty < 0 and duty <= -int(self.min_dut): # Makes sure to get the direction correct
                duty = abs(duty)
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(duty)
                
            elif duty > 0 and duty >= int(self.min_dut):
                self.t3ch1.pulse_width_percent(duty)
                self.t3ch2.pulse_width_percent(0)
                
            elif duty < 0 and duty > -int(self.min_dut):
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(int(self.min_dut))
                
            elif duty > 0 and duty < int(self.min_dut):
                self.t3ch1.pulse_width_percent(int(self.min_dut))
                self.t3ch2.pulse_width_percent(0)
                
            elif duty == 0: # Set duty to 0
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(0)
            else:
                
                pass
            
        elif moe == 2:# checks motor id to see which motor to set duty cycle
            
            self.t3ch3.pulse_width_percent(0)
            self.t3ch4.pulse_width_percent(0)
            
            if duty >= 100:# checks input duty to make sure whithin range
                self.t3ch3.pulse_width_percent(100)
                self.t3ch4.pulse_width_percent(0)
                
            elif duty <= -100:
                self.t3ch3.pulse_width_percent(100)
                self.t3ch4.pulse_width_percent(0)
                
            elif duty < 0 and duty <= -int(self.min_dut): # Makes sure to get the direction correct
                duty = abs(duty)
                self.t3ch3.pulse_width_percent(0)
                self.t3ch4.pulse_width_percent(duty)
                
            elif duty > 0 and duty >= int(self.min_dut):
                self.t3ch3.pulse_width_percent(duty)
                self.t3ch4.pulse_width_percent(0)
                
            elif duty < 0 and duty > -int(self.min_dut):
                self.t3ch3.pulse_width_percent(0)
                self.t3ch4.pulse_width_percent(int(self.min_dut))
                
            elif duty > 0 and duty < int(self.min_dut):
                self.t3ch3.pulse_width_percent(int(self.min_dut))
                self.t3ch4.pulse_width_percent(0)
                
            elif duty == 0:  # Set duty to 0
                self.t3ch3.pulse_width_percent(0)
                self.t3ch4.pulse_width_percent(0)
            else:
                
                pass
        else:
            pass
        

    
if __name__ == '__main__':
    
    ## Sleep pin for the motors
    pin_nSLEEP = pyb.Pin.cpu.A15
    
    ## Pin 1 for Motor 1
    pin_IN1 = pyb.Pin.cpu.B0
    ## Pin 2 for Motor 2
    pin_IN2 = pyb.Pin.cpu.B1
    ## Timer object
    timer = 3
    ## Minimum duty cycle
    min_dut = 30
    ## Pin 1 for Motor 2
    pin_IN12 = pyb.Pin.cpu.B4
    ## Pin 2 for Motor 2
    pin_IN22 = pyb.Pin.cpu.B5
    ## Dual Motor Driver     
    moe = MotorDriver(pin_nSLEEP , pin_IN1 , pin_IN2,pin_IN12,pin_IN22, timer ,  min_dut)

    moe.enable()

    moe.set_duty(30, 1) 

    moe.set_duty(30, 2)

    moe.set_duty(30, 2)

    moe.set_duty(40, 1) 

    
    
    
        
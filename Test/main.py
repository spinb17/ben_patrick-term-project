# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 17:10:41 2021

@author: Patrick Ward
"""

from Motor_Driver import MotorDriver
import pyb
import utime
from Encoder import Encoder

## Stores the Encoder object for the second motor
Enc2 = Encoder(pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 8)

## Stores the Encoder object for the first motor
Enc1 = Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)

## Sleep pin for the motors
pin_nSLEEP = pyb.Pin.cpu.A15

## Pin 1 for Motor 1
pin_IN1 = pyb.Pin.cpu.B0
## Pin 2 for Motor 2
pin_IN2 = pyb.Pin.cpu.B1
## Timer object
timer = 3
## Minimum duty cycle
min_dut = 30
## Pin 1 for Motor 2
pin_IN12 = pyb.Pin.cpu.B4
## Pin 2 for Motor 2
pin_IN22 = pyb.Pin.cpu.B5
## Dual Motor Driver     
moe = MotorDriver(pin_nSLEEP , pin_IN1 , pin_IN2,pin_IN12,pin_IN22, timer ,  min_dut)

moe.enable()

Enc2.update()

Enc1.update()

moe.set_duty(50, 1) 

moe.set_duty(50, 2)

Enc2.update()

Enc1.update()

utime.sleep(10)

Enc2.update()

Enc1.update()

pos2 = str(Enc2.position)
pos1 = str(Enc1.position)

print(pos2)
print(pos1)

moe.set_duty(-50, 2)

moe.set_duty(-50, 1)

Enc2.update()

Enc1.update() 

utime.sleep(10)

Enc2.update()

Enc1.update() 

pos2 = str(Enc2.position)
pos1 = str(Enc1.position)

print(pos2)
print(pos1)

moe.set_duty(0, 2)

moe.set_duty(0, 1)

utime.sleep(10)

Enc2.update()

Enc1.update()

pos2 = str(Enc2.position)
pos1 = str(Enc1.position)

print(pos2)
print(pos1)


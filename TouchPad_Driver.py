'''
@file TouchPad_Driver.py

@brief Driver for reading touch information on our balancing board
@details This driver holds methods for scanning the X Y and Z components of our 
board as well as a methods that reads all components and returns a tuple of their values.
This driver should work to record values from a single channel in less than 500us and 
from all three in less than 1500us. Additionally, permitting a fast enough resonponse, filters 
should be implemented to improve the accuracy of values read

A link to the code is provided at : https://bitbucket.org/spinb17/me405/src/master/Lab_07/TouchPad_Driver.py

@author Ben Spin
@date 3/3/2021

'''
import utime
from pyb import udelay
from pyb import Pin
from pyb import ADC


class TouchDriver:
    ''' 
    @brief This class implements a Touch Pad Driver for the Balancing Platform Board
    '''
    
    def __init__ (self, XP_pin , XM_pin , YP_pin , YM_pin , width , length , center_coordx , center_coordy):
        '''
        @brief Creates a TouchDriver object by initializing with pins and board dimensions
        
        @param XP_pin The Pin choice for XP on our Touch Board
        @param XM_pin The Pin choice for XM on our Touch Board
        @param YP_pin The Pin choice for YP on our Touch Board
        @param YM_pin The Pin choice for YM on our Touch Board
        @param width  The width of the board in mm
        @param legnth The length of the board in mm
        @param center_coordx X coordinate of board center in m
        @param center_Coordy X coordinate of board center in m
        '''
        
        ## Xp pin setup using user input values for the pin
        self.x_p = XP_pin
        
        ## Xm pin setup using user input values for the pin
        self.x_m = XM_pin
        
        ## Yp pin setup using user input values for the pin
        self.y_p = YP_pin
        
        ## Ym pin setup using user input values for the pin
        self.y_m = YM_pin
        
        ## Setting up a variable for the width of the board
        self.width = width
        
        ## Setting up a variable for the length of the board
        self.length = length
        
        ## Setting up a variable for the center coordinate x vaue 
        self.center_X = center_coordx
        
        ## Setting up a variable for the center coordinate y vaue
        self.center_Y = center_coordy
        
        ## ADC Values at Key Points
        
        ## Highest and lowest recorded ADC values from sensor for X axis
        self.adc_x_high = 3680
        self.adc_x_low  = 535
        
         ## Highest and lowest recorded ADC values from sensor for Y axis
        self.adc_y_high = 3800
        self.adc_y_low  = 250
        
        

        ## MM Increments for ADC
        
        ## X incrament calculation
        self.x_inc = (self.adc_x_high - self.adc_x_low)/self.width
        ## Y increament calculation
        self.y_inc = (self.adc_y_high - self.adc_y_low)/self.length
        
        
    def Xread (self):
            '''
            @brief method to call to read the x values of our touch sensor
            @return Method returns x positoin in dimensions of mm
            '''
            
            ## Initializing pin Layout to read X values
            self.y_m.init(mode=Pin.IN)
            self.x_m.init(mode=Pin.OUT_PP , value=0)
            self.y_p.init(mode=Pin.IN)
            self.x_p.init(mode=Pin.OUT_PP , value=1)
            
            ## Waiting some time for stable response
            udelay(4)
                       
            ## Converting to a mm value using incrament determined in init
            self.mm_value_x = (ADC(self.y_m).read()/ self.x_inc ) - self.center_X
            ## Returning mm Value
            return self.mm_value_x
            


    def Yread (self):
            '''
            @brief method to call to read the y values of our touch sensor
            @return Method returns y positoin in dimensions of mm
            '''
            ## Initializing pin Layout to read Y values
            self.y_m.init(mode=Pin.OUT_PP , value=0)
            self.x_m.init(mode=Pin.IN)
            self.y_p.init(mode=Pin.OUT_PP , value=1)
            self.x_p.init(mode=Pin.IN)
            
            ## Waiting some time for stable response
            udelay(4)
            
           
            ## Converting to a mm value using incrament determined in init
            self.mm_value_y = (ADC(self.x_m).read() / self.y_inc ) - self.center_Y 
            ## Returning mm Value
            return self.mm_value_y
            

    def Zread (self):
            '''
            @brief method to call to read if there is contact with sensor
            @return Method returns z position as a True or False for touch
            '''
            
            ## Initializing pin Layout to read contact
            self.y_m.init(mode=Pin.IN)
            self.x_m.init(mode=Pin.OUT_PP , value=0)
            self.y_p.init(mode=Pin.OUT_PP , value=1)
            self.x_p.init(mode=Pin.IN)
            
            ## Waiting some time for stable response
            udelay(4)
                       
            ## Checking for contact values and returning either 1 for yes or 0 for no
            return(ADC(self.y_m).read() < 4000)
            

    def three_comp (self):
            '''
            @brief method to call to get a tuple of values representing the x,y , and contact check of our sensor
            @return Method returns a tuple of (X,Y,Z)
            '''
            ## Initializing pin Layout to read X values
            self.y_m.init(mode=Pin.IN)
            self.x_m.init(mode=Pin.OUT_PP , value=0)
            self.y_p.init(mode=Pin.IN)
            self.x_p.init(mode=Pin.OUT_PP , value=1)
            
            ## Waiting some time for stable response
            udelay(4)
                       
            ## Converting to a mm value using incrament determined in init
            self.mm_value_x = (ADC(self.y_m).read()/ self.x_inc ) - self.center_X 

            
            ## Initializing pin Layout to read contact
            self.y_p.init(mode=Pin.OUT_PP , value=1)
            self.x_p.init(mode=Pin.IN)
            
            ## Waiting some time for stable response
            udelay(4)
                       
            ## Checking for contact values and returning either 1 for yes or 0 for no
            Z = (ADC(self.y_m).read() < 4000)
            
            ## Initializing pin Layout to read Y values
            self.y_m.init(mode=Pin.OUT_PP , value=0)
            self.x_m.init(mode=Pin.IN)
            
            ## Waiting some time for stable response
            udelay(4)
            
           
            ## Converting to a mm value using incrament determined in init
            self.mm_value_y = (ADC(self.x_m).read() / self.y_inc ) - self.center_Y 

            

        
            ## Forming a tuple using previous methods
            return (self.mm_value_x, self.mm_value_y , Z)
            


if __name__ == '__main__':
    
    ## Touchpad Driver object
    Dri =  TouchDriver( Pin.board.PA7 , Pin.board.PA1 , Pin.board.PA0 , Pin.board.PA6 , 172 , 98 , 107 , 53)
    
    while True:
           ## Start time for run time calc
           start_time = utime.ticks_us()
           ## Stores reading for all three data peices 
           reading = Dri.three_comp()
           ## When the three_comp() function finishes running
           end_time = utime.ticks_us()
           ## Run time of the function
           total_time = utime.ticks_diff(end_time , start_time)
           print( str(reading) + 'total time in micro secconds ' + '{:}'.format(total_time))
            
           
            
         
